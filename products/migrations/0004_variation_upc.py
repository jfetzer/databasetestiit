# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0003_productimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='variation',
            name='upc',
            field=models.CharField(default=1234567890, max_length=120),
            preserve_default=False,
        ),
    ]
