from django.shortcuts import render


def about(request):
	return render(request, "about.html", {})

def locations(request):
	return render(request, "maps.html", {})