# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carts', '0004_remove_cartitem_line_item_total'),
        ('orders', '0003_remove_usercheckout_braintree_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('shipping_total_price', models.DecimalField(default=5.99, max_digits=50, decimal_places=2)),
                ('order_total', models.DecimalField(max_digits=50, decimal_places=2)),
                ('billing_address', models.ForeignKey(to='orders.UserAddress', null=True, related_name='billing_address')),
                ('cart', models.ForeignKey(to='carts.Cart')),
                ('shipping_address', models.ForeignKey(to='orders.UserAddress', null=True, related_name='shipping_address')),
                ('user', models.ForeignKey(null=True, to='orders.UserCheckout')),
            ],
        ),
    ]
