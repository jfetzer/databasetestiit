# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_id',
            field=models.CharField(default=True, max_length=120),
        ),
        migrations.AddField(
            model_name='product',
            name='upc',
            field=models.IntegerField(null=True),
        ),
    ]
